package com.awamo.rover;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;

public class CoordinateTests {

    Coordinate coordinate;
    int x= 5;
    int y =10;
    //Test that a point object can be created
    @Before
    public void beforePointTest() {
        coordinate = new Coordinate(x, y);
    }

    @Test
    public void testShouldCreateAnewInstanceOfCoordinate() {
        assertNotNull(coordinate);
    }

    //Test that a cordinate can be created
    @Test
    public void testShoudGetCoordinateXAndYValues() {
        assertThat(String.valueOf((coordinate.getX())), is(String.valueOf(x)));
        assertThat(String.valueOf(coordinate.getY()), is(String.valueOf(y)));

    }

}
