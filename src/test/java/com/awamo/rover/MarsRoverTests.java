package com.awamo.rover;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static java.lang.System.out;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * This class is responsible to send test commands that will move the rover to the desired
 * direction and for the desired number of steps
 */
@RunWith(JUnitParamsRunner.class)
public class MarsRoverTests {
    MarsRover rover;
    Grid grid;

    @Before
    public void setUp() {
        grid = new Grid(10, 10);
        rover = new MarsRover(grid);
    }

    //Test that the rover can move to the left direction
    @Test
    @Parameters({
            "l,0:0:W",
            "ll,0:0:S",
            "lll,0:0:E",
            "llll,0:0:N"
    })
    public void shouldTestDirectionChangeToTheLeft(String moveComand, String position) {
        out.println("Direction Commands Issued: " + moveComand);
        assertThat(rover.issueCommand(moveComand), is(position));
    }

    //Test that the rover can move to the right direction
    @Test
    @Parameters({
            "r,0:0:E",
            "rr,0:0:S",
            "rrr,0:0:W",
            "rrrr,0:0:N"
    })
    public void shouldTestDirectionChangeToTheRight(String moveComand, String position) {
        out.println("Direction Commands Issued: " + moveComand);
        assertThat(rover.issueCommand(moveComand), is(position));
    }

    //Test that the rover can move forward(to the north direction)
    @Test
    @Parameters({
            "f,0:1:N",
            "ffff,0:4:N"
    })
    public void shouldTestMoveForwardVertically_incrementYByNumberOfSteps(String moveComand, String position) {
        out.println("Movement Commands Issued: " + moveComand);
        assertThat(rover.issueCommand(moveComand), is(position));
    }

    //Test that the rover can move forward(to the east direction)
    @Test
    @Parameters({
            "rf,1:0:E",
            "rffff,4:0:E"
    })
    public void shouldTestMoveForwardHorizontally_incrementXByNumberOfSteps(String moveComand, String position) {
        out.println("Movement Commands Issued: " + moveComand);
        assertThat(rover.issueCommand(moveComand), is(position));
    }

    //Test that the rover can move backwards(to the south direction)
    @Test
    @Parameters({
            "rrb,0:9:S",
            "rrbbbb,0:6:S"
    })
    public void shouldTestMoveBackwardVertically_decrementYByNumberOfSteps(String moveComand, String position) {
        out.println("Movement Commands Issued: " + moveComand);
        assertThat(rover.issueCommand(moveComand), is(position));
    }

    //Test that the rover can move backward(to the west direction)
    @Test
    @Parameters({
            "rrrb,9:0:W",
            "rrrbbbb,6:0:W"
    })
    public void shouldTestMoveBackwardHorizontally_decrementXByNumberOfSteps(String moveComand, String position) {
        out.println("Movement Commands Issued: " + moveComand);
        assertThat(rover.issueCommand(moveComand), is(position));
    }
    //Test that the rover can overlap from north to south
    @Test
    @Parameters({
            "ffffffffff,0:0:N"
    })
    public void shouldTestOverlapFromSouthToNorth(String moveComand, String position) {
        out.println("Movement Commands Issued: " + moveComand);
        assertThat(rover.issueCommand(moveComand), is(position));
    }

    //Test that the rover can overlap from south to north
    @Test
    @Parameters({
            "rrffffffffff,0:0:S"
    })
    public void shouldTestOverlapFromNorthToSouth(String moveComand, String position) {
        out.println("Movement Commands Issued: " + moveComand);
        assertThat(rover.issueCommand(moveComand), is(position));
    }

    //Test that the rover can overlap from east to west
    @Test
    @Parameters({
            "rrrffffffffff,0:0:W"
    })
    public void shouldTestOverlapFromEastToWest(String moveComand, String position) {
        out.println("Movement Commands Issued: " + moveComand);
        assertThat(rover.issueCommand(moveComand), is(position));
    }

    //Test that the rover can overlap from west to east
    @Test
    @Parameters({
            "rffffffffff,0:0:E"
    })
    public void shouldTestOverlapFromWestToEast(String moveComand, String position) {
        out.println("Movement Commands Issued: " + moveComand);
        assertThat(rover.issueCommand(moveComand), is(position));
    }

}
