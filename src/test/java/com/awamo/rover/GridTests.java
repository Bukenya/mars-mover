package com.awamo.rover;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class GridTests {
    Grid grid;
    private static final int HEIGHT = 10;
    private static final int WIDTH = 14;

    @Before
    public void beforePointTest() {
        grid = new Grid(WIDTH, HEIGHT);
    }

    @Test
    public void testShouldCreateAnewInstanceOfGrid() {
        assertNotNull(grid);
    }

    //Test that a cordinate can be created
    @Test
    public void testShoudGetGridLengthAndHeight() {
        assertThat(String.valueOf(grid.getGridHeight()), is(String.valueOf(HEIGHT)));
        assertThat(String.valueOf(grid.getGridWidth()), is(String.valueOf(WIDTH)));

    }

}
