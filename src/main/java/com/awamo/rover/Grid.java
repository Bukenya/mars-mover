package com.awamo.rover;

/**
 * This class acts as a grid container that has the move methods and has visibility
 * to the the size of the canvas
 */
public class Grid {
    private int gridHeight;
    private int gridWidth;

    public Grid(int length, int height) {
        this.gridWidth = length;
        this.gridHeight = height;
    }

    /**
     * Function to be used when moving in either direction
     * If the movement is to the left, the x coordinate value is decremented
     * If the movement is to the right, the x coordinate value is incremented
     * If the the movement is to the north, the y coordinate value is incremented
     * If the movement is to the south, the y coordinate value is decremented
     * @param coordinate Coordinate object
     * @param direction Current Direction
     * @return new Coordinate
     */
    public Coordinate move(Coordinate coordinate, Direction direction) {
        int y = coordinate.getY();
        int x = coordinate.getX();

        if (direction == Direction.NORTH) {
            y = ((y + 1) % gridHeight);
        }

        if (direction == Direction.EAST) {
            x = ((x + 1) % gridWidth);
        }
        if (direction == Direction.SOUTH) {
            //Increment the value of y
            y = y > 0 ? y - 1 : gridHeight - 1;
        }

        if (direction == Direction.WEST) {
            //decrement the value of x
            x = x > 0 ? x - 1 : gridWidth - 1;
        }
        return new Coordinate(x, y);
    }

    public int getGridHeight() {
        return gridHeight;
    }

    public int getGridWidth() {
        return gridWidth;
    }
}
