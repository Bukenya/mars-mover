package com.awamo.rover;

public enum Direction {
    DEFAULT_DIRECTION("W", "N", "E"),
    NORTH("W", "N", "E"),
    EAST("N", "E", "S"),
    SOUTH("E", "S", "W"),
    WEST("S", "W", "N");

    private String leftDirection;
    private String currentDirection;
    private String rightDirection;

    Direction(String leftDirection, String currentDirection, String rightDirection) {

        this.leftDirection = leftDirection;
        this.currentDirection = currentDirection;
        this.rightDirection = rightDirection;
    }

    //Return the direction in the left
    public Direction changeDirectiontoTheleft() {
        for (Direction direction : values()) {
            if (direction.currentDirection.equalsIgnoreCase(this.leftDirection)) {
                return direction;
            }
        }
        return Direction.DEFAULT_DIRECTION;
    }

    public Direction changeDirectiontoTheRight() {
        for (Direction direction : values()) {
            if (direction.currentDirection.equalsIgnoreCase(this.rightDirection)) {
                return direction;
            }
        }
        return Direction.DEFAULT_DIRECTION;
    }

    public String getCurrentDirection() {
        return currentDirection;
    }
}
