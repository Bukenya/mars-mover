package com.awamo.rover;

public class MarsRover {

    //Initiate the direction to north
    Direction direction = Direction.NORTH;
    Coordinate coordinate = new Coordinate(0, 0);
    Grid grid;

    public MarsRover(Grid grid) {
        this.grid = grid;
    }

    /**
     * This function acts as the listener for all incoming commands and takes action
     * @param commands What the Rover should do eg f- forward, b -backword, r- turn right, l- turn left
     * @return the new cordinate and direction of the Rover
     */
    public String issueCommand(String commands) {
        //Iterate through the issued command and take action
        for (char command : commands.toCharArray()) {
            if (command == 'l') {
                //change direction to left
                direction = direction.changeDirectiontoTheleft();
            }
            if (command == 'r') {
                //Change to the right direction relative to the current direction
                direction = direction.changeDirectiontoTheRight();
            }
            //Nove the number of steps in the command
            if (command == 'f' || command == 'b') {
                //move forward
                coordinate = grid.move(coordinate, direction);
            }

        }
        return coordinate.getX() + ":" + coordinate.getY() + ":" + direction.getCurrentDirection();
    }

}
