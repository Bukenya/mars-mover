Mard Rover Kata - Polly Bukenya
-----------------------------
Here is the challenge I was attempting
Develop an API that moves a rover around on a grid.<br/>
<ul>
<li>You are given the initial starting point (x,y) of a rover and the direction (N, S, E, W) it is facing.</li>
<li>The rover receives a character array of commands.</li>
<li>Implement commands that move the rover forward/backwards (f,b).</li>
<li>Implement commands that turn the rover left/right (l,r).</li>
<li>Implement wrapping from one edge of the grid to another. (planets are spheres after all)</li> 
</ul>

<br/>

Editor Used
--------------
IntelliJ

Java version
-------------
Java 11 

Other dependencies
-----------------
Mavel
